package Problems;

/**
 * FunProblems - combination of multiple methods that solve different problems.
 * @author Bryan Secoy
 * @version 1.0
 */
public class FunProblems {

    /**
     * main - entry point of the program
     * @param args - not used
     */
    public static void main(String[] args) {
        System.out.println("Run junit tests to verify functionality of test functions.");
    }

    /**
     * reverseArray - Reverses the order of the given int[]
     * @param a - the original int[]
     * @return - the reverses int[]
     */
    public static int[] reverseArray(int[] a) {
        int[] result = new int[a.length];
        int count = 0;
        for (int i = a.length; i > 0; i--){
            result[count] = a[i-1];
            count++;
        }
        return result;
    }

    /**
     * shiftLeft - shifts the characters in the array to the left by n places.
     * @param arr - the original character array
     * @param n - the number of shifts
     * @return - char array shifted to the left by n places
     */
    public static char[] shiftLeft(char[] arr, int n) {

        char[] temp = new char[arr.length];       // temp array for storing shifting values
        for (int i = 0; i < n; i++) {
            char last = arr[arr.length - 1];      // remember the last char
            temp[arr.length -1] = arr[0];         // move first char in arr to last index of temp

            //shift other chars to left by one.
            System.arraycopy(arr, 1, temp, 0, arr.length - 1 - 1);

            temp[arr.length -2] = last;         // write the remembered last value in the correct index
            arr = temp;
        }

        return arr;
    }

    /**
     * shiftLeft - shifts the characters in the array to the left by n places.
     * @param arr - the original character array
     * @param n - the number of shifts
     * @return - char array shifted to the left by n places
     */
    public static char[] shiftRight(char[] arr, int n) {

        char[] temp = new char[arr.length];       // temp array for storing shifting values

        for (int i = 0; i < n; i++) {             // Shifts n times
            char last = arr[arr.length-1];                  // remember the last char

            //shift other chars to right by one.
            System.arraycopy(arr, 0, temp, 1, arr.length - 1);

            temp[0] = last;         // write the remembered first value in the correct index
            System.arraycopy(temp, 0, arr, 0, arr.length);
        }

        return arr;
    }

    /**
     * countSwaps - Counts the number of swap required to sort the given array using the bubblesort algorithm.
     * @param a - The array to be sorted
     * @return swaps - The number of swaps needed to sort the array
     */
    public static int countSwaps(int[] a) {
        int swaps = 0;
        int n =a.length;
        int temp;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n - 1; j++) {
                // Swap adjacent elements if they are in decreasing order
                if (a[j] > a[j + 1]) {
                    swaps++;
                    temp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = temp;
                }
            }
        }
        return swaps;
    }
}
