package Tests;

import Problems.FunProblems;
import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.*;

/**
 * Test class for FunProblems methods.
 * @author Bryan Secoy
 * @version 1.0
 */
public class FunProblemsTest {

    @Test
    public void reverseArrayTestEven(){
        int[] arr = {1,2,3,4};
        int[] result = FunProblems.reverseArray(arr);
        int[] expected = {4,3,2,1};

        for (int i = 0; i < arr.length; i++) {
            assertEquals(result[i], expected[i]);
        }
    }

    @Test
    public void reverseArrayTestOdd(){
        int[] arr = {1,2,3,4,5};
        int[] result = FunProblems.reverseArray(arr);
        int[] expected = {5,4,3,2,1};

        for (int i = 0; i < arr.length; i++) {
            assertEquals(result[i], expected[i]);
        }
    }

    @Test
    public void leftShiftTestTwo() {
        char[] arr = {'a','b','c','d','e'};
        char[] result = FunProblems.shiftLeft(arr, 2);
        char[] expected = {'c','d','e','a','b'};

        assertTrue(Arrays.equals(expected, result));
        assertFalse(Arrays.equals(result, arr));
    }

    @Test
    public void leftShiftTestThree() {
        char[] arr = {'a','b','c','d'};
        char[] result = FunProblems.shiftLeft(arr, 3);
        char[] expected = {'d','a','b','c'};

        assertTrue(Arrays.equals(expected, result));
        assertFalse(Arrays.equals(result, arr));
    }

    @Test
    public void leftShiftTestFullRotation() {
        char[] arr = {'a','b','c','d','e'};
        char[] result = FunProblems.shiftLeft(arr, 20);
        char[] expected = {'a','b','c','d','e'};

        assertTrue(Arrays.equals(expected, result));
        assertTrue(Arrays.equals(result, arr));
    }

    @Test
    public void rightShiftTestTwo() {
        char[] arr = {'a','b','c','d','e'};
        char[] result = FunProblems.shiftRight(arr, 2);
        char[] expected = {'d','e','a','b','c'};

        System.out.println(result);

        assertTrue(Arrays.equals(expected, result));
        assertTrue(Arrays.equals(result, arr));
    }

    @Test
    public void countSwapsTest() {
        int[] arr = {5,8,3,1};
        int result = FunProblems.countSwaps(arr);
        int expected = 5;

        assertEquals(expected, result);
    }
}